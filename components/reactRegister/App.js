import React,{ useState, useEffect } from "react"

const App = () => {
    const apiUrl = 'http://ec2-54-189-61-175.us-west-2.compute.amazonaws.com:8080';
    
    const [isLogged, setIsLogged] = useState(false);
    const [state, setState] = React.useState({
        username: "",
        password: "",
        password2: "",
    });

    useEffect(() => {
        window.addEventListener("onLogout", () => {
            init();
          });
    },[])

    const init = () =>{
        setIsLogged(false);
        setState({  username: "",
            password: "",}
        );
    }
   
    const handleChange = event => {
        const value = event.target.value;
        setState({...state, [event.target.name]: value});
    }

    const sendCredentials = () => {
        fetch(`${apiUrl}/adduser`  , {
            method: 'POST',
            body: JSON.stringify({
                username:state.username,
                password:state.password,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then((response) => response.json())
        .then((user) => {
            setIsLogged(true);
            cerrarReactR();
            window.myapp.userListener(user.id);
        });
    }

    const cerrarReactR = event => {
        $("#vue").removeClass("hide");
        $("#reactR").addClass("hide");
        $("#Register").removeClass("buttonSelected");
    }

    if (isLogged){
        return (
            <>
            </>
        )
    }
    
    return (
        <>     
            <div class="reactR">        
                <input name="username"  class="inputLR" placeholder='Username' value={state.username} onChange={handleChange}></input><br></br>
                <input name="password"  class="inputLR" placeholder='Password' value={state.password} onChange={handleChange} type='password'></input><br></br>
                <input name="password2" class="inputLR" placeholder='Repeat Password' value={state.password} onChange={handleChange} type='password'></input><br></br>

                <div class="btnCont"><button class="button-react" type="button" onClick={sendCredentials}> Register</button></div>
                <button class="button-close" type="button" id="cerrarReactR" onClick={cerrarReactR}>Close</button>
            </div>
        </>
    )
}

export default App