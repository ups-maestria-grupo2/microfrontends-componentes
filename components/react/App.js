import React,{ useEffect, useState } from "react"

const App = () => {
    const apiUrl = 'http://ec2-54-189-61-175.us-west-2.compute.amazonaws.com:8080';
    
    const [isLogged, setIsLogged] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');
    
    const [state, setState] = React.useState({
        username: "",
        password: "",
    });
    
    useEffect(() => {
        window.addEventListener("onLogout", () => {
           init(); 
        });
    },[])

    const init = () =>{
        setIsLogged(false);
        setAlertMessage("");
        setState({  username: "",
            password: "",}
        );
    }

    const handleChange = event => {
        const value = event.target.value;
        setState({...state, [event.target.name]: value});
    }

    const sendCredentials = () => {
        fetch(`${apiUrl}/login`  , {
            method: 'POST',
            body: JSON.stringify({
                username:state.username,
                password:state.password,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then((response) => {
            if (response.ok) return response.json() 
        })
        .then((user) => {
            console.log('user', user);
            if (user) {
                setIsLogged(true);
                cerrarReact();
                window.myapp.userListener(user.id);
            }
            else setAlertMessage('credential incorrecta!');
        });
    }

    const cerrarReact = () => {
        $("#vue").removeClass("hide");
        $("#react").addClass("hide");
        $("#reactR").addClass("hide");        
        $("#LogIn").removeClass("buttonSelected");
    }

    if (isLogged){
        return (
            <>
            </>
        )
    }
    return (
            <>   
                <div class="react">       
                    <input name="username"  class="inputLR" placeholder='Username' value={state.username} onChange={handleChange}></input><br></br>
                    <input name="password"  class="inputLR" placeholder='Password' value={state.password} onChange={handleChange} type='password'></input><br></br>
                    <div class="btnCont">
                        <label>{alertMessage}</label>
                        <button class="button-react" type="button" onClick={sendCredentials}>Login</button>
                    </div>
                    <button class="button-close" type="button" id="cerrarReact" onClick={cerrarReact}>Close</button>
                </div>  
            </>
    );
}

export default App
