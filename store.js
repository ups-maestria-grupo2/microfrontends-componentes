window.myapp = {
    isReact: false,
    isVue: false,
    uId: null,
    message: 'CHAO',
    isLogged: false,
    notify: (message) => {
        window.myapp.message = message;
        window.dispatchEvent(new Event('onNotify'));
    },
    validate: (user, pass) => {
        window.myapp.isLogged = false;
        if(user == pass){
            window.myapp.isLogged = true;
            $("#vue").removeClass("hide");
            $("#react").addClass("hide");
            $("#reactR").addClass("hide");        
            $("#LogIn").removeClass("buttonSelected");
        }
        window.dispatchEvent(new Event('onValidate'));
    },
    userListener: (userId) => {
        window.dispatchEvent(new CustomEvent('onUserListener', {'detail': {
            userId,
        }}));
        $("#vue").removeClass("hide");
    },

    logout: () => {
        window.myapp.isLogged = false;
        window.dispatchEvent(new CustomEvent('onLogout'));
        $("#vue").removeClass("hide");
    }

};


