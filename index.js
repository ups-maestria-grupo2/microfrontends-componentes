$(document).ready(function () {
  $("#LogIn").click(function () {
    $("#vue").addClass("hide");
    $("#react").removeClass("hide");
    $("#reactR").addClass("hide");
    $("#LogIn").addClass("buttonSelected");
    $("#Register").removeClass("buttonSelected");
  });
  $("#Register").click(function () {
    $("#vue").addClass("hide");
    $("#react").addClass("hide");
    $("#reactR").removeClass("hide");
    $("#Register").addClass("buttonSelected");
    $("#LogIn").removeClass("buttonSelected");
  }); 
  $("#log-out").click(function () {
    window.myapp.userListener(null);
    $("#log-in").removeClass("hide");
    $("#log-out").addClass("hide");
  });  
});
